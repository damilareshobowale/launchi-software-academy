<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=1472157559781206&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<header>
    	<!--Top Strip Wrap Start-->
        <div class="top_strip">
        	<div class="container">
                <div class="top_location_wrap">
                    <p><i class="fa fa-map-marker"></i>8B Sejeet Mega Plaza, Opp. Alata, Under G, Ogbomoso, Oyo State.</p>
                </div>
                <div class="top_ui_element">
                    <ul>
                        <li><i class="fa fa-envelope"></i><a href="#">launchisoftwareacademy@gmail.com</a></li>
                        <li><i class="fa fa-phone"></i>+234 703 243 6335, +234 815 890 3727</li> 
                    </ul>
                </div>
            </div>
        </div>
        <!--Top Strip Wrap End-->
        
        <!--Navigation Wrap Start-->
        <div class="logo_nav_outer_wrap">
        	<div class="container">
                <div class="logo_wrap">
                    <a href="#"><img src="images/logo.jpg" alt=""></a>
                </div>
                <nav class="main_navigation">
                    <ul>
                        <li><a href="index.php" class="active">HOME</a></li>
                        <li><a href="courses.php">COURSES</a></li>
                        <li><a href="instructors.php">INSTRUCTORS</a></li>
                        <li><a href="enrol.php">ENROL FOR TRAINING</a></li>
                        <li><a href="about.php">ABOUT US</a></li>
                        <li><a href="contact.php">CONTACT US</a></li>
                    </ul>
                </nav>
                <!--DL Menu Start-->
                <div id="kode-responsive-navigation" class="dl-menuwrapper">
                    <button class="dl-trigger">Open Menu</button>
                    <ul class="dl-menu">
                        <li><a href="index.php" class="active">HOME</a></li>
                        <li><a href="courses.php">COURSES</a></li>
                        <li><a href="instructors.php">INSTRUCTORS</a></li>
                        <li><a href="enrol.php">ENROL FOR TRAINING</a></li>
                        <li><a href="about,php">ABOUT US</a></li>
                        <li><a href="contact.php">CONTACT US</a></li>
                    </ul>
                </div>
                <!--DL Menu END-->
            </div>
        </div>
        <!--Navigation Wrap End-->
    </header>