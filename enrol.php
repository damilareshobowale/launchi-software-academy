<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Enrol for a Course - Launchi Software Academy</title>

    <!-- Favicons-->
    <link rel="icon" href="images/favicon.jpg" sizes="32x32">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bx-Slider StyleSheet CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet"> 
    <!-- Font Awesome StyleSheet CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/svg-style.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- DL Menu CSS -->
	<link href="js/dl-menu/component.css" rel="stylesheet">
    <!-- Widget CSS -->
    <link href="css/widget.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- Shortcodes CSS -->
    <link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
 
  </head>

  <body>

<!--Wrapper Start-->  
<div class="ct_wrapper">
	
    <!--Header Wrap Start-->
    <?php include_once('inc_header.php'); ?>
    <!--Header Wrap End-->
    
    <!--Banner Wrap Start-->
    <section class="sub_banner_wrap">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-6">
                	<div class="sub_banner_hdg">
                    	<h3>Enrol Now</h3>
                    </div>
                </div>
                <div class="col-md-6">
                	<div class="ct_breadcrumb">
                    	<ul>
                        	<li><a href="index.php">Home</a></li>
                            <li><a href="javascript:void(0);">Enrol</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="ct_content_wrap">
        
        <!--Get in Touch With Us Wrap Start-->
        <section>
        	<div class="container">
            	<div class="get_touch_wrap">
                	<h4>SEND US A MESSAGE FOR YOUR REQUEST</h4>
                    <p>Pick a course you would like to kickstart, fill the details below. As soon as we get your request, we will communicate with you for the next step to follow. </p>
                </div>
                <div class="row">
                	<div class="col-md-8">
                        <hr>
                    	<div class="ct_contact_form">
                        	<form action="admin/actionmanager.php" id="requestform" role="form">
                                <div class="form_field" style="padding-bottom: 20px">
                                    <select class="conatct_plchldr" id="course" name="course">
                                        <option value="">Choose Course</option>
                                        <option value="Matlab">Matlab</option>
                                        <option value="Web Design">Web Design</option>
                                        <option value="Web Development">Web Development</option>
                                        <option value="Web Design and Development">Web Design and Development</option>
                                        <option value="Aspen Hysys & Aspen Plus">Aspen Hysys & Aspen Plus</option>
                                        <option value="2D & 3D Animation">2D & 3D Animation </option>
                                        <option value="Networking">Networking</option>
                                        <option value="Proteus">Proteus </option>
                                        <option value="Plant 3D">Plant 3D</option>
                                        <option value="Python">Python</option>
                                        <option value="C++">C++</option>
                                        <option value="C#">C#</option>
                                        <option value="MySQL">MySQL</option>
                                        <option value="PHP">PHP</option>
                                        <option value="Javascript">Javascript</option>
                                        <option value="Django">Django</option>
                                        <option value="Ajax">Ajax</option>
                                        <option value="Photoshop">Photoshop</option>
                                        <option value="Corel Draw">Corel Draw </option>
                                        <option value="Solid Works">Solid Works</option>
                                        <option value="Microsoft Softwares">Microsoft Softwares</option>
                                        <option value="Desktop Publishing">Desktop Publishing</option>
                                        <option value="Web Mockup">Web Mockup</option>
                                        <option value="Graphic Design">Graphic Design</option>
                                    </select>
                                </div>
                                <div class="form_field">
                                    <label class="fa fa-user"></label>
                                    <input class="conatct_plchldr" id="name" name="name" type="text" placeholder="Your Name *">
                                </div>
                                <div class="form_field">
                                    <label class="fa fa-envelope-o"></label>
                                    <input class="conatct_plchldr" id="email" name="email" type="text" placeholder="Email Address *">
                                </div>
                                <div class="form_field">
                                    <label class="fa fa-phone"></label>
                                    <input class="conatct_plchldr" id="phonenumber" name="phonenumber" type="text" placeholder="Phone Number">
                                </div>
                                <div class="form_field">
                                    <button id="requestformbutton" type="submit">Request Now <i class="fa fa-arrow-right"></i> </button>
                                    <div id="result" style="font-weight: bold; font-size: 15px"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
						<div class="bottom_border">
							<div class="row">
								<div class="col-md-6">
									<div class="ct_contact_address">
										<h5><i class="fa fa-map-o"></i>Address</h5>
										<p>8B Sejeet Mega Plaza, Opp. Alata, Under G, Ogbomoso, Oyo States.</p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="ct_contact_address">
										<h5><i class="fa fa-envelope-o"></i>Phone Number & Email</h5>
										<ul class="fax_info">
											<li>+234 703 243 6335</li>
                                            <li>+234 815 890 3727</li>
										</ul>
										<p>launchisoftwareacademy@gmail.com</p>
									</div>
								</div>
							</div>
						</div>
						
                    </div>
                    
                </div>
            </div>
        </section>
        <!--Get in Touch With Us Wrap End-->
        
        
    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    <?php include_once('inc_footer.php'); ?>
    <!--Footer Wrap End-->
        
</div>
<!--Wrapper End-->



    <!--Bootstrap core JavaScript-->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
    <!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.js"></script>
	<!--Dl Menu Script-->
	<script src="js/dl-menu/modernizr.custom.js"></script>
	<script src="js/dl-menu/jquery.dlmenu.js"></script>
	<!--Map JavaScript-->
    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBO5my0LQFXOcB2mi7EHuvJh9EkwluBrnc"></script>
    <!--Time Counter Javascript-->
    <script src="js/jquery.downCount.js"></script>
    <!--Pretty Photo Javascript-->
    <script src="js/jquery.prettyPhoto.js"></script>
    <!--Way Points Javascript-->
    <script src="js/waypoints-min.js"></script>
    <!--Accordian Javascript-->
    <script src="js/jquery.accordion.js"></script>
    <!--Custom JavaScript-->
	<script src="js/custom.js"></script>
    <!-- contact message javascript -->
    <script src="contact.js"></script>

  </body>
</html>
