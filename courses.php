<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Our Courses | Launchi Software Academy</title>

    
     <!-- Favicons-->
    <link rel="icon" href="images/favicon.jpg" sizes="32x32">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bx-Slider StyleSheet CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet"> 
    <!-- Font Awesome StyleSheet CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/svg-style.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- DL Menu CSS -->
    <link href="js/dl-menu/component.css" rel="stylesheet">
    <!-- Widget CSS -->
    <link href="css/widget.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="css/chosen.min.css" rel="stylesheet">
    <!-- Shortcodes CSS -->
    <link href="css/shortcodes.css" rel="stylesheet">
    <!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
 
  </head>

  <body>

<!--Wrapper Start-->  
<div class="ct_wrapper">
    
    <!--Header Wrap Start-->
    <?php include_once('inc_header.php'); ?>
    <!--Header Wrap End-->
    
    <!--Banner Wrap Start-->
    <section class="sub_banner_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="sub_banner_hdg">
                        <h3>Our Courses</h3>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ct_breadcrumb">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Our Courses</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="ct_content_wrap">
        <section>
            <div class="container">
                <div class="search_filter">
                    <h4>ALL COURCES</h4>
                    
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="ct_course_list_wrap">
                            <figure>
                                <img src="https://www.soel.ru/upload/clouds/1/iblock/2c7/2c727de0eae87a072a1afedcca6bc3bb/465439.jpg" alt="matlab_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                        <a href="enrol.php?course=matlab">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                                <h5><a href="#">MATLAB TRAINING</a></h5>
                                <p>MATLAB is the easiest and most productive software environment for engineers and scientists. Learn the basic and advanced tools to become an expert and certified in Matlab</p>
                                <div class="foo_get_qoute">
                                <a href="enrol.php?cours=matlab">ENROL NOW</a>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="ct_course_list_wrap">
                            <figure>
                                <img src="http://www.piegonmedia.com/wp-content/uploads/2016/05/web-design-development-company-Chandigarh.png" alt="website_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                        <a href="enrol.php?course=web">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                                <h5><a href="#">WEBSITE DESIGN AND DEVELOPMENT</a></h5>
                                <p>Web Design encompasses many different skills and disciplines in the production and maintenance of websites.</p>
                                <div class="foo_get_qoute">
                                    <a href="enrol.php?course=web">ENROL NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="ct_course_list_wrap">
                            <figure>
                                <img src="images/python.jpg" alt="python_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                        <a href="enrol.php?course=python">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                                <h5><a href="#">PYTHON PROGRAMMING LANGUAGE</a></h5>
                                <p>Web Design encompasses many different skills and disciplines in the production and maintenance of websites.</p>
                                <div class="foo_get_qoute">
                                    <a href="enrol.php?course=python">ENROL NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="ct_course_list_wrap">
                            <figure>
                                <img src="images/aspen-hysys.jpg" alt="aspen_hysys_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                        <a href="enrol.php?course=aspen">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                                <h5><a href="#">ASPEN HYSYS AND <br>ASPEN PLUS</a></h5>
                                <p>This software tackles the most complex process manufacturing challenges, creating value and improving profitability for our customers.</p>
                                <div class="foo_get_qoute">
                                    <a href="enrol.php?course=aspen">ENROL NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="ct_course_list_wrap">
                            <figure>
                                <img src="images/graphic.png" alt="graphic_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                        <a href="enrol.php?course=graphics">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                                <h5><a href="#">GRAPHIC DESIGN</a></h5>
                                <p>Graphic design is the process of visual communication and problem-solving using one or more of typography, photography and illustration.</p>
                                <div class="foo_get_qoute">
                                    <a href="enrol.php?course=graphics">ENROL NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="ct_course_list_wrap">
                            <figure>
                                <img src="images/2d-3d-animation.png" alt="2d_and_3d_animations">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                        <a href="enrol.php?course=graphics">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                                <h5><a href="#">2D & 3D ANIMATION</a></h5>
                                <p>2 dimensional animations deals more with drawing and framing and is the base of 3D animation. Wish to know how? Enrol Now.</p>
                                <div class="foo_get_qoute">
                                    <a href="enrol.php?course=graphics">ENROL NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


         <!--Courses By Subject Wrap Start-->
        <section class="ct_courses_subject_bg">
            <div class="container">
                <!--Heading Style 1 Wrap Start-->
                <div class="ct_heading_1_wrap ct_white_hdg">
                    <h3>LSA Packages</h3>
                    <span><img src="images/hdg-01.png" alt=""></span>
                </div>
                <!--Heading Style 1 Wrap End-->
                
                <!--Courses Subject List Wrap Start-->
                <div class="courses_subject_carousel owl-carousel">
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_1">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Matlab</span>Training</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_2">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>PHP</span>Programming</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_3">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Web Design </span>and Development</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_1">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Photoshop</span>Softwares</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_4">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Aspen Hysys</span> & Aspen Plus</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_5">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>2D & 3D</span>Animation</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_6">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Network</span>Engineering</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_5">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Auto CAD</span>Design</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_2">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Solid</span>Works</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_1">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Proteus</span>Training</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="course_subject_wrap ct_bg_3">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Plant</span>3D</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_4">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Python</span>Programming</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_5">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>C++</span>Programming</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_6">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>MySQL</span>Programming</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Courses Subject List Wrap End-->
            </div>
        </section>
        <!--Courses By Subject Wrap End-->
    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    <?php include_once('inc_footer.php'); ?>
    <!--Footer Wrap End-->
        
</div>
<!--Wrapper End-->



    <!--Bootstrap core JavaScript-->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--Bx-Slider JavaScript-->
    <script src="js/jquery.bxslider.min.js"></script>
    <!--Owl Carousel JavaScript-->
    <script src="js/owl.carousel.js"></script>
    <!--Dl Menu Script-->
    <script src="js/dl-menu/modernizr.custom.js"></script>
    <script src="js/dl-menu/jquery.dlmenu.js"></script>
    <!--Time Counter Javascript-->
    <script src="js/jquery.downCount.js"></script>
    <!--Pretty Photo Javascript-->
    <script src="js/jquery.prettyPhoto.js"></script>
    <!--Way Points Javascript-->
    <script src="js/waypoints-min.js"></script>
    <!--Accordian Javascript-->
    <script src="js/jquery.accordion.js"></script>
    <!--Accordian Javascript-->
    <script src="js/chosen.jquery.min.js"></script>
    <!--Custom JavaScript-->
    <script src="js/custom.js"></script>

  </body>
</html>
