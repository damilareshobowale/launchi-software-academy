//send message from the contact form
$('#sendmessage').on('submit', function(event)
{

	$('#sendmessagebutton').html("Sending  <i class='fa fa-arrow-right'></i>");

	
	event.preventDefault();

	/* get some values from elements on the page: */
         var $form = $(this),
         	name = $form.find('input[name="name"]').val(),
         	email = $form.find('input[name="email"]').val(),
         	phonenumber = $form.find('input[name="phonenumber"]').val(),
         	message = $form.find('textarea[name="message"]').val(),
            url = $form.attr('action');

	//validates the inputs
	$count = 0;
	$msg = '';
	
	if (name == '')
	{
		$msg += "<br>Your Full name is required";
		$count += 1;
	}
	if (email == '')
	{
		$msg += "<br>Email is required";
		$count += 1;
	}
	if (message == '')
	{
		$msg += "<br>Please write a message";
		$count += 1;
	}
	if ($count != 0)
	{
		$('#result').html("<div id='card-alert' class='card red'>" +
                      "<div class='card-content white-text'>" +
                        "<p style='font-weight: bold; color: #FF0000'> " + $count + " error</p>" +
                        "<small style='color: #FF0000'>" + $msg + " </small>" +
                      "</div>" +
                    "</div>");
		$('#sendmessagebutton').html("Send Now <i class='fa fa-arrow-right'></i>");
		return;
	}


	 /* Send the data using post */
    var posting = $.post(url, {
        name: name,
        email: email,
        phonenumber: phonenumber,
        message: message,
        command: "send_message" 
    });
    
    /* Put the results in a div */
    posting.done(function(data) {
        $("#result").html(data);
        $('#sendmessagebutton').html("Send Now <i class='fa fa-arrow-right'></i>");       
    });

	return;
});


//request course
$('#requestform').on('submit', function(event)
{

	$('#requestformbutton').html("Requesting  <i class='fa fa-refresh'></i>");

	
	event.preventDefault();

	/* get some values from elements on the page: */
         var $form = $(this),
         	name = $form.find('input[name="name"]').val(),
         	email = $form.find('input[name="email"]').val(),
         	phonenumber = $form.find('input[name="phonenumber"]').val(),
         	course = $form.find('select[name="course"]').val(),
            url = $form.attr('action');

	//validates the inputs
	$count = 0;
	$msg = '';
	if (course == '')
	{
		$msg += "<br>Please select a course to enrol";
		$count += 1;
	}
	if (name == '')
	{
		$msg += "<br>Your Full name is required";
		$count += 1;
	}
	if (email == '')
	{
		$msg += "<br>Email is required";
		$count += 1;
	}
	if (phonenumber == '')
	{
		$msg += "<br>Phone Number is required";
		$count += 1;
	}
	if ($count != 0)
	{
		$('#result').html("<div id='card-alert' class='card red'>" +
                      "<div class='card-content white-text'>" +
                        "<p style='font-weight: bold; color: #FF0000'> " + $count + " error</p>" +
                        "<small style='color: #FF0000'>" + $msg + " </small>" +
                      "</div>" +
                    "</div>");
		$('#requestformbutton').html("Request Now <i class='fa fa-arrow-right'></i>");
		return;
	}


	 /* Send the data using post */
    var posting = $.post(url, {
        name: name,
        email: email,
        phonenumber: phonenumber,
        course: course,
        command: "request_course" 
    });
    
    /* Put the results in a div */
    posting.done(function(data) {
        $("#result").html(data);
        $('#requestformbutton').html("Request Now <i class='fa fa-arrow-right'></i>");       
    });

	return;
});