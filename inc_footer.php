<footer>
    	<!--NewsLetter Wrap Start-->
        <div class="ct_newsletter_wrap">
        	<div class="container">
            	<div class="newletter_des">
                	<h5>Subscribe Our Weekly Newsletter</h5>
                    <form>
                    	<label class="fa fa-envelope-o"></label>
                    	<input type="text" placeholder="Enter Your Email">
                        <button>Subscribe Me</button>
                    </form>
                </div>
            </div>
        </div>
        <!--NewsLetter Wrap End-->
        
        <!--Footer Col Wrap Start-->
        <div class="ct_footer_bg">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-3 col-sm-6">
                    	<div class="footer_col_1 widget">
                        	<a href="#"><img src="images/logo.jpg" alt=""></a>
                            <p>Meet our professionals to get trained and become an expert. You can also opt in for one on one mentorship in any of our software packages.</p>
                            <span>Email : launchisoftwareacademy@gmail.com</span>
                            <span>Phone : +234 703 243 6335, +234 815 890 3727</span>
                            <div class="foo_get_qoute">
                            	<a href="contact-us.php">Send Us a Message</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-6">
                    	<div class="foo_col_2 widget">
                        	<h5>Pages</h5>
                            <ul>
                            	<li><a href="index.php">Home</a></li>
                                <li><a href="about-us.php">About Us</a></li>
                                <li><a href="courses.php">Courses</a></li>
                                <li><a href="instructors.php">Our Instructors</a></li>
                                <li><a href="enrol.php">Enrol for a training</a></li>
                                <li><a href="contact-us.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="fb-page" data-href="https://www.facebook.com/launchisoftwareacademy/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/launchisoftwareacademy/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/launchisoftwareacademy/">LS Academy</a></blockquote></div>
                        <a class="twitter-timeline"
                                 href="https://twitter.com/Launchi_Academy">
                        Tweets by @Launchi_Academy
                        </a>
                    </div>
                    
                    
                    <div class="col-md-3 col-sm-6">
                    	<div class="foo_col_4 widget">
                        	<h5>Opening Hours</h5>
                            <ul>
                            	<li>Monday to Friday 8:00am to 6:00pm</li>
                                <li>Saturday 9:00am to 5:00pm</li>
                                <li></li>
                                <li>Sunday: CLOSED</li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!--Footer Col Wrap End-->
        
        <!--Footer Copyright Wrap Start-->
        <div class="ct_copyright_bg">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-6">
                    	<div class="copyright_text">
                        	Built with <a class="fa fa-heart" href="#"></a> from <a href="#">Shobowale</a>. <?php echo date('Y'); ?>. All right reserved.
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<div class="copyright_social_icon">
                        	<ul>
                            	<li><a href="https://www.linkedin.com/in/fajimi-lanrewaju-i-launchi-ab1bb3a6/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            	<li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            	<li><a href="https://www.facebook.com/launchisoftwareacademy/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Footer Copyright Wrap End-->
        <div class="back_to_top">
            <a href="#"><i class="fa fa-angle-up"></i></a>
        </div>
    </footer>