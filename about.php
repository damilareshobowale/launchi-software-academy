<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>About Us | Launchi Software Academy</title>

     <!-- Favicons-->
    <link rel="icon" href="images/favicon.jpg" sizes="32x32">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bx-Slider StyleSheet CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet"> 
    <!-- Font Awesome StyleSheet CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/svg-style.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- DL Menu CSS -->
	<link href="js/dl-menu/component.css" rel="stylesheet">
    <!-- Widget CSS -->
    <link href="css/widget.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- Shortcodes CSS -->
    <link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
 
  </head>

  <body>

<!--Wrapper Start-->  
<div class="ct_wrapper">
	
    <!--Header Wrap Start-->
   <?php include_once('inc_header.php'); ?>
    <!--Header Wrap End-->
    
    <!--Banner Wrap Start-->
    <section class="sub_banner_wrap">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-6">
                	<div class="sub_banner_hdg">
                    	<h3>About LSA</h3>
                    </div>
                </div>
                <div class="col-md-6">
                	<div class="ct_breadcrumb">
                    	<ul>
                        	<li><a href="#">Home</a></li>
                            <li><a href="#">About us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="ct_content_wrap">
        <!--Get Started Wrap Start-->
        <section>
        	<div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="get_started_content_wrap ct_blog_detail_des_list">
                            <h3>We are TEAM OF PROFESSIONALS ready to take your CAREER to the next level.</h3>
                            <p>Launchi Software Academy is a team of experts in the software development industry ready to train individuals (beginner, intermediates or professionals) in any software programming languages and become a success wherever you find yourself. We also do one on one mentorship to individals that will prefer a close matching tutorial. Our Instructors are not just expert but also with high emotional intelligence to understand the needs of the students. <br>
                                You can also consult us for all ICT solutions individually or as a company. We help to build industrial based software projects depending on the needs of the industry. <br>
                                Feel free to consult us if you need more information. </p>
                            <ul>
                                <li>We are home to 500 students and 20 experts.</li>
                                <li>We provide personal and industry based software and hardware solutions.</li>
                                <li>We also offer ICT services and registrations.</li>
                                <li>We offer home, sessions and one on one mentorship classes.</li>
                                <li>Our office is very convenient and conducive to all.</li>
                            </ul>
                        </div>
                    </div>
                
                    <div class="col-md-6">
                        <div class="get_started_video">
                                <img src="images/group-anniversary.jpg" alt="ANNIVERSARY_ONCE">
                                
                            </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Get Started Wrap End-->
        
        
        
        <!--Our Teacher Wrap Start-->
        <section class="teacher_bg">
        	<div class="container">
            	<!--Heading Style 1 Wrap Start-->
                <div class="ct_heading_1_wrap">
                	<h3>Our Instructors</h3>
                    <p>Our instructors are team of experts with high level of practical knowledge plus emotional intelligence. We don't just teach, we give practical solutions.</p>
                    <span><img src="images/hdg-01.png" alt=""></span>
                </div>
                <!--Heading Style 1 Wrap End-->
                
                <!--Teacher List Wrap Start-->
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="ct_teacher_outer_wrap">
                            <figure>
                                <img alt="Fajimi_Lanre_CEO" src="images/lanre_ceo.jpg">
                            </figure>
                            <div class="ct_teacher_wrap">
                                <h5><a href="#">FAJIMI LANRE</a></h5>
                                <span>CEO / Matlab</span>
                                <ul>
                                    <li><a href="https://www.facebook.com/Launchi4real" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/godwinshobowale" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/fajimi-lanrewaju-i-launchi-ab1bb3a6/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="ct_teacher_outer_wrap">
                            <figure>
                                <img alt="Shobowale_Damilare_Elijah" src="images/damilare.jpg">
                            </figure>
                            <div class="ct_teacher_wrap">
                                <h5><a href="#">SHOBOWALE DAMILARE</a></h5>
                                <span>Instructor / PHP & C++ </span>
                                <ul>
                                    <li><a href="https://www.facebook.com/dshobowale" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/oluwadamilare-shobowale-3bb84b108/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="ct_teacher_outer_wrap">
                            <figure>
                                <img alt="Bello_Folayemi" src="images/bello.jpg">
                            </figure>
                            <div class="ct_teacher_wrap">
                                <h5><a href="#">BELLO FOLAYEMI</a></h5>
                                <span>Instructor / Python</span>
                                <ul>
                                    <li><a href="https://www.facebook.com/iholhuwharphyhorlharyhemmiebaylowzee" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/bello-folayemi-77548ba5/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="ct_teacher_outer_wrap">
                            <figure>
                                <img alt="Ojesanmi_Seyi" src="images/seyi.jpg">
                            </figure>
                            <div class="ct_teacher_wrap">
                                <h5><a href="#">OJESANMI SEYI</a></h5>
                                <span>Instructor / Networking & PHP</span>
                                <ul>
                                    <li><a href="https://www.facebook.com/ojesanmi.seyi" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/jerryseyi"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/ojesanmi-jerry-seyi-77886984/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Teacher List Wrap End-->
                
            </div>
        </section>
        <!--Our Teacher Wrap End-->
        
        <!--Learn More Wrap Start-->
        <div class="ct_learn_more_bg">
        	<div class="container">
            	<div class="ct_learn_more">
                	<h4>We kickstart individuals' careers to become an <span>expert.</span></h4>
                    <a href="contact.php">Contact Us</a>
                </div>
            </div>
        </div>
        <!--Learn More Wrap End-->
        
        <!--About Us Services Wrap Start-->
        
        <!--About Us Services Wrap End-->
        
        <!--Testimonial Wrap Start-->
        <section class="ct_testimonial_bg">
        	<div class="container">
            	<!--Heading Style 1 Wrap Start-->
                <div class="ct_heading_1_wrap ct_white_hdg">
                	<h3>Cources By Subject</h3>
                    <p>Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consec <br/>tetuer adipis elit, aliquam eget nibh etlibura.</p>
                    <span><img src="images/hdg-01.png" alt=""></span>
                </div>
                <!--Heading Style 1 Wrap End-->
                
                <!--Testimonial List Wrap Start-->
                <div class="testimonial_carousel owl-carousel">
                	<div class="item">
                    	<div class="testimonial_wrap">
                        	<figure>
                            	<img src="extra-images/testimonial-01.png" alt="">
                            </figure>
                            <p>They exceeded my expectations and did so with professionalism and integrity. They are organized, intelligent, efficient and keep up to date. with the ever changing world of SEO. 
I highly recommend them."</p>
						<span><b>Fiona Lara</b>, DY Physics</span>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimonial_wrap">
                        	<figure>
                            	<img src="extra-images/testimonial-01.png" alt="">
                            </figure>
                            <p>They exceeded my expectations and did so with professionalism and integrity. They are organized, intelligent, efficient and keep up to date. with the ever changing world of SEO. 
I highly recommend them."</p>
						<span><b>Fiona Lara</b>, DY Physics</span>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimonial_wrap">
                        	<figure>
                            	<img src="extra-images/testimonial-01.png" alt="">
                            </figure>
                            <p>They exceeded my expectations and did so with professionalism and integrity. They are organized, intelligent, efficient and keep up to date. with the ever changing world of SEO. 
I highly recommend them."</p>
						<span><b>Fiona Lara</b>, DY Physics</span>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimonial_wrap">
                        	<figure>
                            	<img src="extra-images/testimonial-01.png" alt="">
                            </figure>
                            <p>They exceeded my expectations and did so with professionalism and integrity. They are organized, intelligent, efficient and keep up to date. with the ever changing world of SEO. 
I highly recommend them."</p>
						<span><b>Fiona Lara</b>, DY Physics</span>
                        </div>
                    </div>
                </div>
                <!--Testimonial List Wrap End-->
                
            </div>
        </section>
        <!--Testimonial Wrap End-->
        
        <!--Faqs and Term Wrap Start-->
       
        <!--Faqs and Term Wrap End-->
        
    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    <?php include_once('inc_footer.php'); ?>
    <!--Footer Wrap End-->
        
</div>
<!--Wrapper End-->



    <!--Bootstrap core JavaScript-->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
    <!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.js"></script>
    <!--Time Counter Javascript-->
    <script src="js/jquery.downCount.js"></script>
	<!--Dl Menu Script-->
	<script src="js/dl-menu/modernizr.custom.js"></script>
	<script src="js/dl-menu/jquery.dlmenu.js"></script>
    <!--Pretty Photo Javascript-->
    <script src="js/jquery.prettyPhoto.js"></script>
    <!--Way Points Javascript-->
    <script src="js/waypoints-min.js"></script>
    <!--Accordian Javascript-->
    <script src="js/jquery.accordion.js"></script>
    <!--Custom JavaScript-->
	<script src="js/custom.js"></script>

  </body>
</html>
