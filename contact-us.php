<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Contact Us - Launchi Software Academy</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bx-Slider StyleSheet CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet"> 
    <!-- Font Awesome StyleSheet CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/svg-style.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- DL Menu CSS -->
	<link href="js/dl-menu/component.css" rel="stylesheet">
    <!-- Widget CSS -->
    <link href="css/widget.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- Shortcodes CSS -->
    <link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
 
  </head>

  <body>

<!--Wrapper Start-->  
<div class="ct_wrapper">
	
    <!--Header Wrap Start-->
    <?php include_once('inc_header.php'); ?>
    <!--Header Wrap End-->
    
    <!--Banner Wrap Start-->
    <section class="sub_banner_wrap">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-6">
                	<div class="sub_banner_hdg">
                    	<h3>Send Us a Message</h3>
                    </div>
                </div>
                <div class="col-md-6">
                	<div class="ct_breadcrumb">
                    	<ul>
                        	<li><a href="index.php">Home</a></li>
                            <li><a href="javascript:void(0);">Contact us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="ct_content_wrap">
        <!--Map Wrap Start-->
        <div class="map-canvas gt_contact_us_map" id="map-canvas"></div>
        <!--Map Wrap End-->
        
        <!--Get in Touch With Us Wrap Start-->
        <section>
        	<div class="container">
            	<div class="get_touch_wrap">
                	<h4>GET IN TOUCH WITH US:</h4>
                    <p>Let us know what you want us to do and we will give you how best we can help you to archieve your career. Send us a message amd we will get in touch with you as soon as possible. </p>
                </div>
                <div class="row">
                	<div class="col-md-6">
                    	<div class="ct_contact_form">
                        	<form action="admin/actionmanager.php" id="sendmessage" role="form">
                            	<div class="form_field">
                                	<label class="fa fa-user"></label>
                                	<input class="conatct_plchldr" id="name" name="name" type="text" placeholder="Your Name *">
                                </div>
                                <div class="form_field">
                                    <label class="fa fa-envelope-o"></label>
                                    <input class="conatct_plchldr" id="email" name="email" type="text" placeholder="Email Address *">
                                </div>
                                <div class="form_field">
                                	<label class="fa fa-phone"></label>
                                	<input class="conatct_plchldr" id="phonenumber" name="phonenumber" type="text" placeholder="Phone Number">
                                </div>
                                <div class="form_field">
                                	<label class="fa fa-edit"></label>
                                	<textarea class="conatct_plchldr" id="message" name="message" placeholder="Write Detail *"></textarea>
                                </div>
                                <div class="form_field">
                                	<button id="sendmessagebutton" type="submit">Send Now <i class="fa fa-arrow-right"></i> </button>
                                    <div id="result" style="font-weight: bold; font-size: 15px"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
						<div class="bottom_border">
							<div class="row">
								<div class="col-md-6">
									<div class="ct_contact_address">
										<h5><i class="fa fa-map-o"></i>Address</h5>
										<p>8B Sejeet Mega Plaza, Opp. Alata, Under G, Ogbomoso, Oyo States.</p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="ct_contact_address">
										<h5><i class="fa fa-envelope-o"></i>Fax & Email</h5>
										<ul class="fax_info">
											<li>+234 703 243 6335</li>
										</ul>
										<p>launchisoftwareacademy@gmail.com</p>
									</div>
								</div>
							</div>
						</div>
						
                        <div class="newletter_des contact_us_newsltr">
                            <h5>Subscribe Our Weekly Newsletter</h5>
                            <form>
                                <label class="fa fa-envelope-o"></label>
                                <input type="text" placeholder="Enter Your Email">
                                <button>Signup</button>
                            </form>
                            <p>Stay in touch with us as we send you free classes and seminars that can kickstart your career. We promise not to spam you! </p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!--Get in Touch With Us Wrap End-->
        
        
    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    <?php include_once('inc_footer.php'); ?>
    <!--Footer Wrap End-->
        
</div>
<!--Wrapper End-->



    <!--Bootstrap core JavaScript-->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
    <!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.js"></script>
	<!--Dl Menu Script-->
	<script src="js/dl-menu/modernizr.custom.js"></script>
	<script src="js/dl-menu/jquery.dlmenu.js"></script>
	<!--Map JavaScript-->
    <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBO5my0LQFXOcB2mi7EHuvJh9EkwluBrnc"></script>
    <!--Time Counter Javascript-->
    <script src="js/jquery.downCount.js"></script>
    <!--Pretty Photo Javascript-->
    <script src="js/jquery.prettyPhoto.js"></script>
    <!--Way Points Javascript-->
    <script src="js/waypoints-min.js"></script>
    <!--Accordian Javascript-->
    <script src="js/jquery.accordion.js"></script>
    <!--Custom JavaScript-->
	<script src="js/custom.js"></script>
    <!-- contact message javascript -->
    <script src="contact.js"></script>

  </body>
</html>
