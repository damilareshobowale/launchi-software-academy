<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Instructors | Launchi Software Academy</title>

    
    <!-- Favicons-->
    <link rel="icon" href="images/favicon.jpg" sizes="32x32">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bx-Slider StyleSheet CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet"> 
    <!-- Font Awesome StyleSheet CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/svg-style.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- DL Menu CSS -->
	<link href="js/dl-menu/component.css" rel="stylesheet">
    <!-- Widget CSS -->
    <link href="css/widget.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- Shortcodes CSS -->
    <link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
    
  
  </head>

  <body>

<!--Wrapper Start-->  
<div class="ct_wrapper">
	
    <!--Header Wrap Start-->
    <?php include_once('inc_header.php'); ?>
    <!--Header Wrap End-->
    
    <!--Banner Wrap Start-->
    <section class="sub_banner_wrap">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-6">
                	<div class="sub_banner_hdg">
                    	<h3>Our Professionals</h3>
                    </div>
                </div>
                <div class="col-md-6">
                	<div class="ct_breadcrumb">
                    	<ul>
                        	<li><a href="index.php">Home</a></li>
                            <li><a href="javascript:void(0);">Instructors</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="ct_content_wrap">
    	<section>
        	<div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                    	<div class="ct_teacher_outer_wrap">
                        	<figure>
                            	<img alt="Fajimi_Lanre_CEO" src="images/lanre_ceo.jpg">
                            </figure>
                            <div class="ct_teacher_wrap">
                            	<h5><a href="#">FAJIMI LANRE</a></h5>
                                <span>CEO / IT Specialist</span>
                                <ul>
                                	<li><a href="https://www.facebook.com/Launchi4real" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/godwinshobowale" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/fajimi-lanrewaju-i-launchi-ab1bb3a6/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                    	<div class="ct_teacher_outer_wrap">
                        	<figure>
                            	<img alt="Shobowale_Damilare_Elijah" src="images/damilare.jpg">
                            </figure>
                            <div class="ct_teacher_wrap">
                            	<h5><a href="#">SHOBOWALE DAMILARE</a></h5>
                                <span>Instructor / Full Stack Dev </span>
                                <ul>
                                	<li><a href="https://www.facebook.com/dshobowale" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/oluwadamilare-shobowale-3bb84b108/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                    	<div class="ct_teacher_outer_wrap">
                        	<figure>
                            	<img alt="Bello_Folayemi" src="images/bello.jpg">
                            </figure>
                            <div class="ct_teacher_wrap">
                            	<h5><a href="#">BELLO FOLAYEMI</a></h5>
                                <span>Instructor / Python</span>
                                <ul>
                                	<li><a href="https://www.facebook.com/iholhuwharphyhorlharyhemmiebaylowzee" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/bello-folayemi-77548ba5/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                    	<div class="ct_teacher_outer_wrap">
                        	<figure>
                            	<img alt="Ojesanmi_Seyi" src="images/seyi.jpg">
                            </figure>
                            <div class="ct_teacher_wrap">
                            	<h5><a href="#">OJESANMI SEYI</a></h5>
                                <span>Instructor / Networking & PHP</span>
                                <ul>
                                	<li><a href="https://www.facebook.com/ojesanmi.seyi" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/jerryseyi"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/ojesanmi-jerry-seyi-77886984/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="ct_teacher_outer_wrap">
                            <figure>
                                <img alt="Temidayo_Graphics" src="images/temidayo.jpeg">
                            </figure>
                            <div class="ct_teacher_wrap">
                                <h5><a href="#">OLUWASEUN TEMIDAYO</a></h5>
                                <span>INSTRUCTOR / GRAPHICS</span>
                                <ul>
                                    <li><a href="javascript:void(0);" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="javascript:void(0);" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="ct_teacher_outer_wrap">
                            <figure>
                                <img alt="Temidayo_Graphics" src="images/ezekiel.jpg">
                            </figure>
                            <div class="ct_teacher_wrap">
                                <h5><a href="#">ADEGOKE EZEKIEL</a></h5>
                                <span>INSTRUCTOR / GRAPHICS</span>
                                <ul>
                                    <li><a href="javascript:void(0);" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="javascript:void(0);" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="javascript:void(0);" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    <?php include_once('inc_footer.php'); ?>
    <!--Footer Wrap End-->
        
</div>
<!--Wrapper End-->



    <!--Bootstrap core JavaScript-->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
    <!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.js"></script>
    <!--Time Counter Javascript-->
    <script src="js/jquery.downCount.js"></script>
    <!--Pretty Photo Javascript-->
    <script src="js/jquery.prettyPhoto.js"></script>
	<!--Dl Menu Script-->
	<script src="js/dl-menu/modernizr.custom.js"></script>
	<script src="js/dl-menu/jquery.dlmenu.js"></script>
    <!--Way Points Javascript-->
    <script src="js/waypoints-min.js"></script>
    <!--Accordian Javascript-->
    <script src="js/jquery.accordion.js"></script>
    <!--Custom JavaScript-->
	<script src="js/custom.js"></script>

  </body>
</html>
