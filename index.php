<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Welcome to Launchi Software Academy</title>

    <!-- Favicons-->
    <link rel="icon" href="images/favicon.jpg" sizes="32x32">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bx-Slider StyleSheet CSS -->
    <link href="css/jquery.bxslider.css" rel="stylesheet"> 
    <!-- Font Awesome StyleSheet CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/svg-style.css" rel="stylesheet">
    <!-- Pretty Photo CSS -->
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <!-- Widget CSS -->
    <link href="css/widget.css" rel="stylesheet">
    <!-- DL Menu CSS -->
	<link href="js/dl-menu/component.css" rel="stylesheet">
    <!-- Typography CSS -->
    <link href="css/typography.css" rel="stylesheet">
    <!-- Animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- Shortcodes CSS -->
    <link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
    <link href="style.css" rel="stylesheet">
    <!-- Color CSS -->
    <link href="css/color.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="css/responsive.css" rel="stylesheet">
 
  </head>

  <body>

<!--Wrapper Start-->  
<div class="ct_wrapper">
	
    <!--Header Wrap Start-->
    <?php include_once('inc_header.php'); ?>
    <!--Header Wrap End-->
    
    <!--Banner Wrap Start-->
    <div class="banner_outer_wrap">
    	<ul class="">
        	<li>
            	<img src="images/background-picture.jpg" alt="">
                <div class="ct_banner_caption">
                	<h4 class="">WELCOME TO <span>LAUNCHI SOFTWARE ACADEMY</span></h4>
                    <span class="">Here Is a Chance to Learn</span>
                    <h2 class="">Hello there, <br/>Are You  Ready to Kickstart Your Career?</h2>
                    <a class="active" href="courses.php"> YES, LET ME FIND COURSES </a>
                    <a class="" href="contact.php" style="color: #000000; font-weight: bold">LOCATE US</a>
                </div>
            </li>
        </ul>
    </div>
    <!--Banner Wrap End-->
    
    <!--Content Wrap Start-->
    <div class="ct_content_wrap">
       
        
        <!--Courses By Subject Wrap Start-->
        <section class="ct_courses_subject_bg">
        	<div class="container">
            	<!--Heading Style 1 Wrap Start-->
                <div class="ct_heading_1_wrap ct_white_hdg">
                	<h3>LSA Packages</h3>
                    <span><img src="images/hdg-01.png" alt=""></span>
                </div>
                <!--Heading Style 1 Wrap End-->
                
                <!--Courses Subject List Wrap Start-->
                <div class="courses_subject_carousel owl-carousel">
                	<div class="item">
                        <div class="course_subject_wrap ct_bg_1">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Matlab</span>Training</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_2">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>PHP</span>Programming</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_3">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Web Design </span>and Development</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_1">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Photoshop</span>Softwares</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_4">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Aspen Hysys</span> & Aspen Plus</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_5">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>2D & 3D</span>Animation</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_6">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Network</span>Engineering</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_5">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Auto CAD</span>Design</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_2">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Solid</span>Works</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_1">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Proteus</span>Training</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="course_subject_wrap ct_bg_3">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Plant</span>3D</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_4">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>Python</span>Programming</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_5">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>C++</span>Programming</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="course_subject_wrap ct_bg_6">
                            <i class="fa fa-briefcase"></i>
                            <div class="course_subject_des">
                                <p><span>MySQL</span>Programming</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Courses Subject List Wrap End-->
            </div>
        </section>
        <!--Courses By Subject Wrap End-->
        
        <!--Most Popular Courses Wrap Start-->
        <section>
        	<div class="container">
            	<!--Heading Style 1 Wrap Start-->
                <div class="ct_heading_1_wrap">
                	<h3>Most Popular Courses</h3>
                    <p>Here are the list of most popular courses enrolled by most of our students.</p>
                    <span><img src="images/hdg-01.png" alt=""></span>
                </div>
                <!--Heading Style 1 Wrap End-->
                
                <!--Most Popular Course List Wrap Start-->
                <div class="most_popular_courses owl-carousel">
                	<div class="item">
                    	<div class="ct_course_list_wrap">
                        	<figure>
                            	<img src="https://www.soel.ru/upload/clouds/1/iblock/2c7/2c727de0eae87a072a1afedcca6bc3bb/465439.jpg" alt="matlab_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                    	<a href="enrol.php?course=matlab">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                            	<h5><a href="#">MATLAB TRAINING</a></h5>
                                <p>MATLAB is the easiest and most productive software environment for engineers and scientists. Learn the basic and advanced tools to become an expert and certified in Matlab</p>
                                <div class="foo_get_qoute">
                                <a href="enrol.php?cours=matlab">ENROL NOW</a>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="ct_course_list_wrap">
                        	<figure>
                            	<img src="http://www.piegonmedia.com/wp-content/uploads/2016/05/web-design-development-company-Chandigarh.png" alt="website_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                    	<a href="enrol.php?course=web">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                            	<h5><a href="#">WEBSITE DESIGN AND DEVELOPMENT</a></h5>
                                <p>Web Design encompasses many different skills and disciplines in the production and maintenance of websites.</p>
                                <div class="foo_get_qoute">
                                    <a href="enrol.php?course=web">ENROL NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="ct_course_list_wrap">
                            <figure>
                                <img src="images/python.jpg" alt="python_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                        <a href="enrol.php?course=python">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                                <h5><a href="#">PYTHON PROGRAMMING LANGUAGE</a></h5>
                                <p>Web Design encompasses many different skills and disciplines in the production and maintenance of websites.</p>
                                <div class="foo_get_qoute">
                                    <a href="enrol.php?course=python">ENROL NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="ct_course_list_wrap">
                            <figure>
                                <img src="images/aspen-hysys.jpg" alt="aspen_hysys_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                        <a href="enrol.php?course=aspen">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                                <h5><a href="#">ASPEN HYSYS AND <br>ASPEN PLUS</a></h5>
                                <p>This software tackles the most complex process manufacturing challenges, creating value and improving profitability for our customers.</p>
                                <div class="foo_get_qoute">
                                    <a href="enrol.php?course=aspen">ENROL NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="ct_course_list_wrap">
                            <figure>
                                <img src="images/graphic.png" alt="graphic_training">
                                <figcaption class="course_list_img_des">
                                    <div class="ct_zoom_effect"></div>
                                    <div class="ct_course_link">
                                        <a href="enrol.php?course=graphics">ENROL NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="popular_course_des">
                                <h5><a href="#">GRAPHIC DESIGN</a></h5>
                                <p>Graphic design is the process of visual communication and problem-solving using one or more of typography, photography and illustration.</p>
                                <div class="foo_get_qoute">
                                    <a href="enrol.php?course=graphics">ENROL NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Most Popular Course List Wrap End-->
                
            </div>
        </section>
        <!--Most Popular Courses Wrap End-->
        
        
        
       
        
      
        <!--Our Events Wrap Start-->
        
        <!--Our Events Wrap End-->
        
        <!--Testimonial Wrap Start-->
        
        <!--Learn More Wrap Start-->
        <div class="ct_learn_more_bg">
        	<div class="container">
            	<div class="ct_learn_more">
                	<h4>We kickstart individuals' careers to become an <span>expert.</span></h4>
                    <a href="contact-us.php">Contact Us</a>
                </div>
            </div>
        </div>
        <!--Learn More Wrap End-->
        
        <!--Latest News Wrap Start-->
       
        <!--Latest News Wrap End-->
    </div>
    <!--Content Wrap End-->
    
    <!--Footer Wrap Start-->
    <?php include_once('inc_footer.php'); ?>
    <!--Footer Wrap End-->
        
</div>
<!--Wrapper End-->



    <!--Bootstrap core JavaScript-->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
    <!--Dl Menu Script-->
	<script src="js/dl-menu/modernizr.custom.js"></script>
	<script src="js/dl-menu/jquery.dlmenu.js"></script>
    <!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.js"></script>
    <!--Time Counter Javascript-->
    <script src="js/jquery.downCount.js"></script>
    <!--Pretty Photo Javascript-->
    <script src="js/jquery.prettyPhoto.js"></script>
    <!--Way Points Javascript-->
    <script src="js/waypoints-min.js"></script>
    <!--Custom JavaScript-->
	<script src="js/custom.js"></script>

  </body>
</html>
