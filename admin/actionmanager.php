<?php

//require_once("inc_dbfunctions.php");
require_once("config.php");

$actionmanager = New ActionManager();

if(isset($_POST['command']) && $_POST['command'] == 'send_message')
{
    $actionmanager->send_message();
}
elseif(isset($_POST['command']) && $_POST['command'] == 'request_course')
{
    $actionmanager->request_course();
}

/**
 * 
 */
class ActionManager
{
	
	function send_message()
	{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$phonenumber = $_POST['phonenumber'];
		$message = $_POST['message'];

		//send the message
		$message = "<div>
						<p> Hello admin, ".$name."sent a message was sent from the contact form of the website, please find details here: </p><br>
						<p>Name: ". $name."</p>
						<p>Email: ".$email."</p>
						<p>Phone Number: ".$phonenumber."</p>
						<p>Message: ".$message."</p>
						<br><br>
						Thank you Admin.
					</div>";
		$replymessage = "<div>
							<p> Hello ".$name." Your message has been received. We will definitely get back to you as soon as possible. If by any means we didn't get back to you, please ensure you call us through <strong> +234 703 243 6335.</strong> </p>
							<br><br>
							Thank You for contacting us. <br>
							Launchi Software Academy
							</div>";

		//send the message
		if(sendEmail('launchisoftwareacademy@gmail.com', 'New Message Received | Launchi Software Academy', $message) && sendEmail($email, 'Dear '.$name.', Your Message has been delivered', $replymessage))
		{
			echo "<span style='color: green;'> Your Message Delivered Successfully. Please check your Inbox. We will get back to you as soon as possible.</span>
				<script type='text/javascript'>
					$('#name').val();
					$('#email').val();
					$('#phonenumber').val();
					$('#message').val();
				</script>";
			return;
		}

	else 
	{
		echo "<span style='color: #ff0000;'> An error occured while sending message, please try again. </span>";
		return;
		
	}

	//also send the message to the applicants

	return;

	}


	//request course online
	function request_course()
	{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$phonenumber = $_POST['phonenumber'];
		$course = $_POST['course'];

		//send the message
		$message = "<div>
						<p> Hello admin, ".$name." request for a course from the website. Please attend to it. </p><br>
						<p>Name: ". $name."</p>
						<p>Email: ".$email."</p>
						<p>Phone Number: ".$phonenumber."</p>
						<p>Course Requested: ".$course."</p>
						<br><br>
						Thank you Admin.
					</div>";
		$replymessage = "<div>
							<p> Hello ".$name." Your request has been received. We will definitely get back to you as soon as possible. If by any means we didn't get back to you, please ensure you call us through <strong> +234 703 243 6335.</strong> </p>
							<br><br>
							Thank You for contacting us. <br>
							Launchi Software Academy
							</div>";

		//send the message
		if(sendEmail('launchisoftwareacademy@gmail.com', 'New Course Request | Launchi Software Academy', $message) && sendEmail($email, 'Dear '.$name.', Your Request has been delivered', $replymessage))
		{
			echo "<span style='color: green;'> Your Message Delivered Successfully. Please check your Inbox. We will get back to you as soon as possible.</span>
				<script type='text/javascript'>
					$('#name').val();
					$('#email').val();
					$('#phonenumber').val();
					$('#message').val();
				</script>";
			return;
		}

	else 
	{
		echo "<span style='color: #ff0000;'> An error occured while sending message, please try again. </span>";
		return;
		
	}

	//also send the message to the applicants

	return;

	}
}



?>